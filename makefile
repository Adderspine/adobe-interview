deploy:
	zip revenue_report_hit_level_data.zip main.py
	aws s3 --profile adobe-interview cp ./revenue_report_hit_level_data.zip s3://adobe-interview/functions/revenue_report_hit_level_data.zip
	aws lambda create-function --cli-input-yaml file://lambda_configs/config.yaml

update:
	zip revenue_report_hit_level_data.zip main.py
	aws s3 --profile adobe-interview cp ./revenue_report_hit_level_data.zip s3://adobe-interview/functions/revenue_report_hit_level_data.zip
	aws lambda update-function-code --cli-input-yaml file://lambda_configs/update_config.yaml

send_data:
	aws s3 --profile adobe-interview cp data/data.sql s3://adobe-interview/data/inputs/

get_output:
	aws s3 --profile adobe-interview cp  s3://adobe-interview/data/output/2021/02/01/2020-02-01_SearchKeywordPerformance.tab data/output/2021/02/01/2020-02-01_SearchKeywordPerformance.tab