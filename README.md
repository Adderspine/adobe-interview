## Description
A transformation function that is triggered when a new file is added to the s3 location s3://adobe-interview/data/inputs/.  This is an example of how to generate the Search Revenue Report for Hit Level Data.  The output is loaded to s3://adobe-interview/data/outputs/YYYY/MM/DD/YYYY-MM-DD_SearchKeywordPerformance.tab for the date that the file was generated.

It is a function that is looking for a TSV input and has an output of a TSV.

## Setup
### Python 3.7
pip install -r requirements.txt

## Deployment
* New deploy: `make deploy`
  * The function is zipped and loaded to s3. Then aws is told of the function.
* Update deployment: `make update`
  * The function is zipped and loaded to s3. Then aws is told that the function has been updated.
* testing: `pytest`
