import codecs
import csv
from datetime import datetime
from functools import reduce
from itertools import groupby
from tempfile import NamedTemporaryFile
from typing import Dict, List
from urllib.parse import urlparse, parse_qs

import boto3
from botocore.exceptions import ClientError

s3_client = boto3.client('s3')


def get_s3_locations(event) -> List[Dict]:
    records = event.get('Records', [])
    return [{
        'bucket': i['s3'].get('bucket', {}).get('name', ''),
        'key': i['s3'].get('object', {}).get('key', ''),
    } for i in records if 's3' in i]


class HitLevelData:

    def __init__(self, ip: str, user_agent: str):
        '''
        :param ip: ip for the hit_level_data record
        :param user_agent: user agent for hit_level_data record
        '''
        self.ip = ip
        self.user_agent = user_agent

        self.search_datetime = None
        self.first_referrer = None

        self._revenue = None

    def __hash__(self) -> int:
        return hash(self.ip.lower() + self.user_agent.lower())

    def add_hit(
            self,
            date_time: str,
            event_list: str,
            product_list: str,
            referrer: str):

        # Look for the earliest hit by user to find which search engine was being used
        # Checking for /search in the url but this might not always be the case when other search engines are used
        hit_time = datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S')
        if (self.search_datetime or datetime.max) > hit_time and\
                '/search' in referrer:
            self.search_datetime = hit_time
            self.first_referrer = referrer

        if '1' in event_list.split(','):
            # [Category];{Product Name];[Number of Items];[Total Revenue];[Custom Event]|[Custom Event];[Merchandizing eVar],.
            try:
                self._revenue = float(product_list.split(';')[3])
            except ValueError as e:
                print(e)

    # Query string parameters for each of the search engines for keywords
    search_key_query_string = {
        'search.yahoo.com': 'p',
        'www.bing.com': 'q',
        'www.google.com': 'q',
    }

    @property
    def domain(self) -> str:
        return '' if self.first_referrer is None else '.'.join(urlparse(
            self.first_referrer
        ).netloc.split('.')[1:])

    @property
    def search_keyword(self) -> str:
        page_url = urlparse(self.first_referrer)
        # If this result returns none an error should be logged.  This record or the file might have issues that need
        # to be resolved before continuing.
        return str(parse_qs(page_url.query).get(
            self.search_key_query_string.get(page_url.netloc, 'k'),
            # The default keyword is None in case no search keyword was given or a search engine wasn't configured
            [None]
        )[0])

    @property
    def revenue(self) -> float:
        # This property is just for convenience and if in the future any additional cleanup needs to happen.
        return self._revenue


def transform_data(tsv_reader):
    # Dictionary is for keeping track of unique users.  This helps when the data is out of order and doesn't require it
    # to be presorted to function correctly. By using a hash to identify the objects it is quick to know if we have
    # already seen hit level data before for a user.  If a user does exist then the the row of data can be attributed to
    # the same user.
    hit_level_data: Dict[int, HitLevelData] = {}
    # transform data
    for row in tsv_reader:
        ip = row[3]
        user_agent = row[2]
        date_time = row[1]
        event_list = row[4]
        product_list = row[10]
        referrer = row[11]

        hld = HitLevelData(ip, user_agent)
        if hash(hld) not in hit_level_data:
            hit_level_data[hash(hld)] = hld
        hit_level_data[hash(hld)].add_hit(
            date_time,
            event_list,
            product_list,
            referrer
        )
    return hit_level_data


def generate_revenue_report_data(hit_level_data):
    # Group by needs data sorted in order to run properly
    report_data = sorted(hit_level_data.values(), key=lambda x: (x.domain, x.search_keyword))
    return sorted([
        (
            key[0],
            key[1],
            # Handling a search that didn't end in revenue by allowing Null
            reduce(lambda a, b: None if b.revenue is None else a + b.revenue, group, 0)
        )
        for key, group in groupby(report_data, lambda x: (x.domain, x.search_keyword))
        # 'or 0' puts the nulls at the end.
    ], key=lambda x: x[2] or 0, reverse=True)


def main(event: dict, context):
    # Get key from event
    locations = get_s3_locations(event)
    for i in locations:
        with NamedTemporaryFile(mode='w+') as save_f:

            # load the file from s3
            obj = s3_client.get_object(Bucket=i['bucket'], Key=i['key'])
            tsv_reader = csv.reader(codecs.getreader("utf-8")(obj['Body']), delimiter="\t")

            # header: This is to ignore the first row
            try:
                next(tsv_reader)
            except StopIteration as e:
                print(e)
                print("There was no data provided in the file")
                exit(0)
            hit_level_data = transform_data(tsv_reader)

            # write data to temporary file
            header = ['Search Engine Domain', 'Search Keyword', 'Revenue']
            tsv_writer = csv.writer(save_f, delimiter="\t")
            tsv_writer.writerow(header)
            data = generate_revenue_report_data(hit_level_data)
            tsv_writer.writerows(data)
            save_f.seek(0, 0)
            # save the file back to s3
            try:
                s3_client.upload_file(save_f.name, i['bucket'], f'data/outputs/{datetime.now().strftime("%Y/%m/%d/%Y-%m-%d")}_SearchKeywordPerformance.tab')
            except ClientError as e:
                print(e)


if __name__ == '__main__':
    main({}, None)
